# 查询
## 拼接Filter
```json
{ "_id": 1, "color": "red", "qty": 5, "vendor": ["A"] }
{ "_id": 2, "color": "purple", "qty": 10, "vendor": ["C", "D"] }
{ "_id": 3, "color": "blue", "qty": 8, "vendor": ["B", "A"] }
{ "_id": 4, "color": "white", "qty": 6, "vendor": ["D"] }
{ "_id": 5, "color": "yellow", "qty": 11, "vendor": ["A", "B"] }
{ "_id": 6, "color": "pink", "qty": 5, "vendor": ["C"] }
{ "_id": 7, "color": "green", "qty": 8,"vendor": ["B", "C"] }
{ "_id": 8, "color": "orange", "qty": 7, "vendor": ["A", "D"] }
```

```kotlin
data class PaintOrder(
    @BsonId val id: Int,
    val qty: Int,
    val color: String,
    val vendors: List<String> = mutableListOf()
)
```

![[Pasted image 20231106150149.png]]


![[Pasted image 20231106150239.png]]
```kotlin
	val orComparison = Filters.or(
		Filters.gt(PaintOrder::qty.name, 8),
		Filters.eq(PaintOrder::color.name, "pink")
	)
	val resultsFlow = collection.find(orComparison)
	resultsFlow.collect { println(it) }
```