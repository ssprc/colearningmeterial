
# 连接
使用@Configuration，注册Database为组件，
```kotlin
import com.mongodb.kotlin.client.coroutine.MongoClient

data class Jedi(val name: String, val age: Int)

// Replace the placeholder with your MongoDB deployment's connection string
val uri = CONNECTION_STRING_URI_PLACEHOLDER

val mongoClient = MongoClient.create(uri)

val database = mongoClient.getDatabase("test")
// Get a collection of documents of type Jedi
val collection = database.getCollection<Jedi>("jedi")
```

```kotlin
@Configuration
open class MongoConfig {

    @Value("\${spring.data.mongodb.uri}")
    private lateinit var mongoUri: String

    @Bean
    open fun mongoClient(): MongoClient {
        return MongoClient.create(mongoUri)
    }

    @Bean
    open fun mongoDatabase(mongoClient: MongoClient): MongoDatabase {
        val dbName = "test"
        return mongoClient.getDatabase(dbName)
    }
}
```

# Model类
```kotlin
@Serializable
data class LightSaber(
    @SerialName("_id") // Use instead of @BsonId
    @Contextual val id: ObjectId?,
    val color: String,
    val qty: Int,
    @SerialName("brand")
    val manufacturer: String = "Acme" // Use instead of @BsonProperty
)
```


# Repo
```kotlin
@Repository
class LightSaberRepository(
    private val mongoDatabase: MongoDatabase
) {

    private val mongoCollection: MongoCollection<LightSaber> = mongoDatabase.getCollection("LightSabers")


    suspend fun insert(LightSaber: LightSaber) {
        mongoCollection.insertOne(LightSaber)
    }

    suspend fun findByName(name: String): List<LightSaber> {
        return mongoCollection.find(eq("name", name)).toList()
    }

    suspend fun findByAgeLessThan(age: Int): List<LightSaber> {
        return mongoCollection.find(lt("age", age)).toList()
    }

    suspend fun updateAgeByName(name: String, age: Int) {
        val filter = eq("name", name)
        val update = set("age", age)
        mongoCollection.updateOne(filter, update)
    }

    suspend fun deleteByName(name: String) {
        val filter = eq("name", name)
        mongoCollection.deleteOne(filter)
    }
}
```



# 增

# 删

# 改


# 查
## 关于返回值
查询结果只有一个的话，那就是类型？的返回值
处理异常的时候，类似如下代码：
```kotlin

```

### 一个条件
