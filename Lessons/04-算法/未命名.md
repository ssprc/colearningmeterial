```java
 public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sq = scanner.nextInt();

        String[][] area = new String[sq + 1][sq + 1];

        ArrayList<Entry> noList = new ArrayList<Entry>();
        for (int i = 1; i <= sq; i++) {
            String[] s = scanner.nextLine().split(" ");
            for (int j = 1; j <= sq; j++) {
                area[i][j] = s[j];
                if ("NO".equals(s[j])) {
                    noList.add(new Entry(i, j));
                }
            }
        }
        //1. 遍历整个NOList，判断能不能成为target(四周有YES) 需要判断数组越界
        //如果不能---- 不用做任何操作
        //如果可以---- 移出NoList 存入target
        //2. 将targetList中的所有都设置为YES
        //如果一次遍历后targetList里没东西了但是NoList内还有，判定为不能完全 -1
        //如果NoList内没东西了，说明成功 返回遍历的次数
        ArrayList<Entry> target = new ArrayList<Entry>();
        int days = 0;
        int result = 0;
        do {
            target.clear();
            for (Entry entry : noList) {
                if ("YES".equals(area[entry.x -1][entry.y]) || "YES".equals(area[entry.x + 1][entry.y]) ||"YES".equals(area[entry.x][entry.y - 1]) || "YES".equals(area[entry.x][entry.y + 1])) {
                    noList.remove(entry);
                    target.add(entry);
                }
            }

            for (Entry entry : target) {
                area[entry.x][entry.y] = "YES";
            }
            days++;
            if (noList.isEmpty()) {
                result = days;
                break;
            }
        } while (!target.isEmpty());

        if (!noList.isEmpty()) {
            result = -1;
        }
        System.out.println(result);
    }
```