```java
package com.guor.od;

import java.util.*;

public class OdTest {
    /**
     * 不能出现两个同学紧挨着，必须隔至少一个空位。
     * 其中0表示没有占位，1表示占位
     *
     * 也就是说最理想的排座是1,0,1,0,1,0,1
     *
     * 如果是0,0,1,0,0,1,0,0,0,1 -- 变成1,0,1,0,0,1,0,1,0,1即可
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 给你一个整数数组desk，表示当前座位的占座情况，由若干个0和1组成，其中0表示没有占位，1表示占位。
        int[] desk = Arrays.stream(sc.nextLine().split(",")).mapToInt(Integer::parseInt).toArray();

        // 能坐几个人
        int num = 0;
        for (int i = 0; i < desk.length; i++) {
            // 如果当前值是0，前一个和后一个都是0，则增加1个座位
            if (desk[i] == 0) {
                // 如果是第一个，就不用判断前一个
                boolean startFlag = (i == 0) || (desk[i - 1] == 0);
                // 如果是最后一个，就不用判断下一个
                boolean endFlag = (i == desk.length - 1) || (desk[i + 1] == 0);
                // 前一个和后一个都是0，则增加1个座位
                if (startFlag && endFlag) {
                    num++;
                    desk[i] = 1;
                    i++;
                }
            }
        }

        // 输出数值表示还能坐几个人
        System.out.println(num);
    }
}

```