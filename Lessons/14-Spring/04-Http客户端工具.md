

## 关于RestTemplate

`RestTemplate` 是 Spring 框架提供的一个用于同步 HTTP 客户端操作的工具。使用 `RestTemplate`，你可以轻松地发出 HTTP 请求并接收响应。它是 Spring 早期版本中常用的 HTTP 客户端工具，并自动集成了许多特性，如错误处理、消息转换器等。

以下是 `RestTemplate` 的一些主要功能：

1. **简化常见HTTP操作**：对于常见的HTTP操作，如GET、POST、PUT、DELETE，`RestTemplate` 提供了简洁的方法。

2. **对象序列化/反序列化**：通过消息转换器（例如：`MappingJackson2HttpMessageConverter` 对于JSON），它可以自动将请求体转换为JSON（或其他格式），并将响应自动反序列化为Java对象。

3. **错误处理**：对于HTTP错误状态（如 404 或 500），`RestTemplate` 抛出特定的异常，这可以简化错误处理。

4. **连接和读取超时**：你可以为 `RestTemplate` 设置超时值，以避免请求挂起太长时间。

5. **拦截器**：你可以添加拦截器来拦截请求/响应，这很有用，比如在发送请求前添加认证头部。

6. **与Spring Security集成**：如果你的应用使用 Spring Security，`RestTemplate` 可以与其集成，自动处理诸如认证等安全关注点。

7. **扩展性**：尽管 `RestTemplate` 为我们提供了许多内置功能，但你仍然可以通过自定义消息转换器、错误处理器等来扩展其功能。

例子：
```java
RestTemplate restTemplate = new RestTemplate();
String result = restTemplate.getForObject("https://api.example.com/data", String.class);
```

尽管 `RestTemplate` 在许多应用中非常有用，但需要注意的是，在Spring 5中，

推荐使用非阻塞的 `WebClient` 作为其替代，尤其是在响应式编程模型中。



## WebClient

```kotlin
open class HttpMethods(val restTemplate: RestTemplate, val urlPre: String) {

    inline fun <B, reified T> generalPost(
        path: String,
        headerList: HeaderList,
        body: B,
        queries: Map<String, Any> = mapOf(),
    ): T {
        var url = "$urlPre/${path.dropWhile { it == '/' }}"
        val header = with(HttpHeaders()) {
            headerList.forEach { add(it.first, it.second) }
            this
        }
        val entity = HttpEntity(body, header)
        if (queries.isNotEmpty()) {
            url += "?" + queries.map { (k, _) -> "$k={$k}" }.joinToString("&")
        }

        return restTemplate.postForObject(url, entity, T::class.java, queries) as T
    }

    inline fun <reified T> generalGet(
        path: String,
        headerList: HeaderList,
        queries: Map<String, Any> = mapOf(),
    ): T {
        var url = "$urlPre/${path.dropWhile { it == '/' }}"
        val header = with(HttpHeaders()) {
            headerList.forEach { add(it.first, it.second) }
            this
        }
        if (queries.isNotEmpty()) {
            url += "?" + queries.map { (k, _) -> "$k={$k}" }.joinToString("&")
        }
        val entity = HttpEntity(null, header)
        // todo handle exception
        return restTemplate.exchange(url, HttpMethod.GET, entity, T::class.java, queries).body as T
    }

}

```

> 使用WebClinet进行改写

```kotlin
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono

open class HttpMethods(val webClient: WebClient) {

    inline fun <reified B, reified T> generalPost(
        path: String,
        headerList: HeaderList,
        body: B,
        queries: Map<String, Any> = mapOf(),
    ): T {
        var uriBuilder = webClient.uriBuilderFactory.builder()
            .path(path)
            .scheme("http")

        queries.forEach { (k, v) -> uriBuilder = uriBuilder.queryParam(k, v) }

        return webClient.post()
            .uri(uriBuilder.build())
            .headers { it.setAll(headerList.toMap()) }
            .bodyValue(body)
            .retrieve()
            .bodyToMono<T>()
            .block() ?: throw RuntimeException("Response is empty")
    }

    inline fun <reified T> generalGet(
        path: String,
        headerList: HeaderList,
        queries: Map<String, Any> = mapOf(),
    ): T {
        var uriBuilder = webClient.uriBuilderFactory.builder()
            .path(path)
            .scheme("http")

        queries.forEach { (k, v) -> uriBuilder = uriBuilder.queryParam(k, v) }

        return webClient.get()
            .uri(uriBuilder.build())
            .headers { it.setAll(headerList.toMap()) }
            .retrieve()
            .bodyToMono<T>()
            .block() ?: throw RuntimeException("Response is empty")
    }
}

typealias HeaderList = List<Pair<String, String>>

```

1. 替换 `RestTemplate` 为 `WebClient`。`WebClient` 的构建和配置在此处省略，假定已经在其他地方配置好。
2. 使用 `WebClient` 的 `uriBuilderFactory` 和 Kotlin 的扩展函数简化 URI 的构建过程。
3. 使用 `bodyToMono<T>()` 方法获取响应体。这里用的是 `block()` 方法来同步等待结果。在响应式编程中，你也可以返回 `Mono<T>` 或 `Flux<T>` 而不是 `T`，从而使整个流程都是响应式的。