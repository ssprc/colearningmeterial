## 基本概念

### 虚拟化技术

### 容器化技术

### 镜像和容器


## 看Docker的版本

## docker下载一个镜像的过程

![image-20231108112100149](https://typoraryne.oss-cn-beijing.aliyuncs.com/undefinedimage-20231108112100149.png)

> 当执行docker run hello-world的时候
>
> 1. 从本地仓库中找镜像
> 2. 找不到就去官方仓库（Docker Hub）去拉取最新的镜像
> 3. 再重新执行run指令

![hello-world【 】](https://typoraryne.oss-cn-beijing.aliyuncs.com/undefinedhello-world%E3%80%90%20%E3%80%91.png)
