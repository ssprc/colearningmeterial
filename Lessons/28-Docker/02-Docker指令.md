# 总览
1. docker run
2. docker run imageName ls:查看这个文件系统的目录
3. docekr ps
# docker run
从镜像创建、执行一个容器
![[hello-world【 】.png]]
执行docker run的时候，会将文件系统快照挪进资源区，执行指令的时候，需要一些资源，会向资源系统申请。得到的结果会返回给进程
## **override 默认指令**
```bash
# docker run <image-name> <command>

docker run busybox echo hello
docker run busybox echo bye
```
查看busybox的**文件系统快照**
```bash
docker run busybox ls
# bin
# dev
# etc
# ...
```
busybox拥有相对完整的linux环境
![[busybox【 】.png]]
但是helloworld没有什么指令（文件系统的根目录），它只是一个编译好的程序
![[hello-world【 】 1.png]]


# docker ls
# docker ps
列举出正在运行的docker
相当于是**docker container ls**
![[Pasted image 20231108131929.png]]
**区别：**
*docker ps* 查看运行中的容器
*docker ps -a* 查看所有容器（包括以前运行的）

# docker 的生命周期

### docker run 和docker start的区别
docker run = docker create + docker start
1. docker create是将image镜像的文件放到系统的资源管理区
2. docker start 才是真正的执行命令了
![create](file:///F:/BaiduNetdiskDownload/docker%E6%96%87%E6%A1%A3/2.docker%20client/assests/create%E3%80%90%20%E3%80%91.png?lastModify=1699432186)


 `docker start -a ${container id}`
-a 代表attach，将docker的输出打印到控制台
不使用-a的话，就会和docker create 一样，回应一下这个container的id

示例指令：
docker create busybox ping aws.com


如果执行两遍docker run hello-world
就会创建两个容器，docker ps -a 里面会有两条记录
如果docker create hello-world
然后start两次，就只会产生一条记录
因为它不会创建新的容器

**启动了之后，有没有别的方法去改变它的状态？**
1. 可以将一个已经终止的container重新执行起来，会重新执行启动docker的指令，此时不能覆盖指令

**docker 可以自己收集日志:**
docker logs containerId
docker logs   
docker -f logs

# 终止docker
docker stop containerId
docker kill  containerId
这两个指令所含的信号量不同，

stop可以有一个优雅退出的时间，让你容器的程序自己退出，而kill是直接退出
stop执行之后10s内如果还不退出，那么将执行kill指令














