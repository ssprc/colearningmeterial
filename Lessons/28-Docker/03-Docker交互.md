# 使用exec指令
**使用docker让两个指令在同一环境（容器）下运行**
在docker执行 docker run redis
此时在linux上执行docker run redis-cli 不能运行成功，因为它默认监听的是本地的6379
![[Pasted image 20231108173232.png]]
*`docker exec <option> <container> <command> `*
![[redis-cli【 】.png]]
这里我们需要docker exec
```shell
 # docker exec <option> <container> <command>  
 # e.g. docker exec -it ......  
 ​  
 docker exec -it redis redis-cli  
 # > set myvalue 2023  
 # OK  
 # > get myvalue  
 # 2023
```


## -it参数
如果不加-it参数，执行完了会直接返回终端
相当于是-t -i
-i  进入交互模式（interactive）
-t 进入终端（TeleTypewriter）
![[stdin【 】.png]]

**如果感觉exec太麻烦，不想切来切去怎么办？**
# 使用shell
什么壳和核，不知道啥意思
docker

如果需要一直与docker进行交互，但是又不想反复使用docker exec指令，我们可以直接打开shell
```shell
docker exec -it <redis container id> sh
# ls
# cd /home
```
可以把shell设为启动命令
```shell
docker run -it busybox sh
```

我们可以借此确认docker的隔离机制。在第一个窗口中启动busybox
```shell
docker run -it busybox sh  
# touch busybox1  
# ls
```


在第2个窗口中同样启动busybox
```shell
docker run -it busybox sh  
# ls
```


默认情况下不同的docker进程相互隔离