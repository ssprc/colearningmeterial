用户在向authox注册申请一个权限的时候，需要带上userName, Authox会为它分配一个密钥，其实是jwt转换过来的。返回给waterloo时候，waterloo会做一个映射，将这个token转换成一个sessionId，(每一次登录可能都不一样)，waterloo会存储它们之间的映射。浏览器在每次发请求的时候（携带不携带可能是由前端控制的），将sessionId放入请求头里。

waterloo接收到sessionId的时候，会将其转换成token，再去转发请求

Authox利用Spring做了一个类似于AOP（推测）的东西，对请求参数进行校验，如果没有这个sessionId，可能会做一些处理。

> 这样一来，携带name信息的token就会被存放在Spring的Context里，想获取的时候就可以直接调用authox提供的方法来获取


# JWT
