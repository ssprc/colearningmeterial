
```json
{
  "_id": "62d8b57b3d38ba21629f0f89",
  "edsId": "62d8b57b3d38ba21629f0f89",
  "name": "水电站信息",        eds名称
  "description": "水电站信息",    eds描述
  "createTime": {    
    "$date": "2022-07-21T02:10:03.539Z"  创建时间
  },
  "updateTime": {
    "$date": "2022-07-21T02:10:29.148Z"   更新时间
  },
  "schema": {
    "fields": [
      {
        "name": "ID",        名称
        "description": "",   字段描述
        "type": {
          "typeClass": "VARCHAR", 对象类型
          "len": -1,          char数据长度
		  "maxLen": 255,       varchar数据长度
          "scale": -1,        decimal长度
          "precision": -1,    decimal精度
          "nullable": false,  是否可空
          "unique": false      是否为主键
        }
      }
    ],
    "keys": [     主键
      "ID"
    ],
    "attachments": []   
  },
  "schemaFieldExt": [
    {
      "CHINESE_NAME": "唯一编号"     中文名称
    },
    {
      "CHINESE_NAME": "名称"
    }
  ],
  "hasDul": false,
  "hasSnapshot": true,
  "extension": {
    "DURATION": [
      "2022-07-21 10:07:26",   有效期
      "2022-07-30 00:00:00"
    ],
    "PHONE": "13925673334",
    "LABEL": [],  标签：（分类）
    "TYPE": "类型一",   
    "CONTACT": "王立",  联系人
    "SYS": "水利局"    所属系统
  },
  "source": null,    
  "deleted": true
}
```

EdsAccessGrant
```json
{
  "_id": {
    "$oid": "62f4a86b5e4d1c00683b5c92"
  },
  "principal": "users",
  "pathAsString": "692e9606-4cfb-4991-a857-3bee18a59bb4",
  "access": [
    "LIST"
  ]
}
```


数据对应关系：DEP
```json
{
	"orgName": "舟山",  //机构名称
	"mobileNumber": "15326896521",  //手机号
	"phoneNumber": "",

	"id": "f2f67a82864849c7a7466d23666fafa6",  //用户id
	"remarks": "",
	"delFlag": "0",  //0 未删除  1删除
	"page": null, //无用字段
	"loginName": "10041", //登录名
	"password": null, //无用字段
	"name": "10041", //姓名
	"sex": "0",  //0男 1女
	"postId": null,  //岗位id
	"email": "15326896521@163.com",  //邮箱
	"photo": "http://10.10.120.142:12307/1eb6bc27-af20-4034-bff0-70d0866c5719.jpg", //用户头像
	"accountStatus": "1", //账号状态 0禁用 1启用 2注销
	"orgId": "9f408b10861f4f818ad233676af413c9",  //机构id
	"postName": null,  //岗位名称
	"createName": null,
	"isManager": "0",  //是否是主管  1:是 0:不是
	"userCode": "202101181003350135", //用户编号
	
	"orgCode": "001008009",  //机构code
	"lastLoginTime": "2021-01-18T14:03:27.000+0800",  //最近登录时间   
	"createBy": "1",  //创建id
	"createDate": "2020-11-04T04:06:06.000+0800",  //创建时间
	"updateBy": "1",  //更新id
	"updateDate": "2021-01-18T10:03:35.000+0800",  //更新时间
}
```
deh.Dep.\_id      id
deh.Dep.name   单位名称   可能是"orgName"
deh.Dep.descriptions.account   用户名（用来登录的）  "loginName"
deh.Dep.descriptions.department  单位名称（和deh.Dep.name 是一样） 
deh.Dep.descriptions.contact   联系人    人名，     "name": "10041"  （如果这个是人名的话）
deh.Dep.descriptions.phone   联系电话   "mobileNumber" 或者是 "phoneNumber"

deh.phantom_dep.\_id    "loginName"
deh.phantom_dep.userId   "id"
deh.phantom_dep.department       "orgName"
deh.phantom_dep.contact        "name"(人名)
deh.phantom_dep.phone               ""phoneNumber"或者"mobileNumber"
deh.phantom_dep.status          本来是为审批所做，也可以显示启用注销等状态。只不过需要传进来字符串 “accountStatus”
deh.phantom_dep.genType        生成类型
deh.phantom_dep.modifiedTime   "updateDate"
deh.phantom_dep.createTime     "createDate"

新增：
deh.phantom_dep.remarks
deh.phantom_dep.createBy
deh.phantom_dep.updateBy
deh.phantom_dep.delFlag
deh.phantom_dep.sex
deh.phantom_dep.postId
deh.phantom_dep.email
deh.phantom_dep.photo
deh.phantom_dep.postName
deh.phantom_dep.isManager
deh.phantom_dep.userCode
deh.phantom_dep.lastLoginTime



在authoxUser中
account：账号
displayname: 单位名称
phonemobile: 联系电话
modifytimestamp: