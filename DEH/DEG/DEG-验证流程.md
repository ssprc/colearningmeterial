---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
DES ^U76GEfrS

DES
Outlier ^L6Qlctve

DEP ^nHeuoLm0

Signed
Request ^wlOJI0UY

Contains:
    1.memId
    2.DegId
    3.Signature ^zvZanJFU

check ^91eqXdmc

TDH ^0W6qy5SV

publicKey ^4kRP3qka

as ^783j3D7m

DES
Inside ^KVSv2ihn

Check
Availability ^wbc8S5EW

catalog and eds ^TdRKJXF9

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://github.com/zsviczian/obsidian-excalidraw-plugin/releases/tag/1.9.25",
	"elements": [
		{
			"type": "rectangle",
			"version": 320,
			"versionNonce": 805994235,
			"isDeleted": false,
			"id": "3edVTvIJqN5ZM8icIVSx7",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -1255.6382665537988,
			"y": -415.40169824571734,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 269.60194314071606,
			"height": 195.7225967791049,
			"seed": 662163675,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "UlbTfwHFHuHT6xNfDbzim",
					"type": "arrow"
				},
				{
					"id": "UjkeSu2VCAUxbUcXFLgXo",
					"type": "arrow"
				}
			],
			"updated": 1698112308605,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 119,
			"versionNonce": 267178517,
			"isDeleted": false,
			"id": "U76GEfrS",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -1131.3685994780503,
			"y": -328.86400536209976,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 41.23997497558594,
			"height": 25,
			"seed": 1514353659,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1698112310708,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "DES",
			"rawText": "DES",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "DES",
			"lineHeight": 1.25,
			"baseline": 17
		},
		{
			"type": "arrow",
			"version": 1117,
			"versionNonce": 1253988891,
			"isDeleted": false,
			"id": "UlbTfwHFHuHT6xNfDbzim",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -982.0110565276982,
			"y": -346.04492838707375,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 468.3042862053502,
			"height": 3.1066332804563217,
			"seed": 1742058139,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1698112370568,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "3edVTvIJqN5ZM8icIVSx7",
				"focus": -0.2979627566537899,
				"gap": 4.02526688538444
			},
			"endBinding": {
				"elementId": "L6Qlctve",
				"focus": -0.9931511989132026,
				"gap": 8.206325863480515
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					468.3042862053502,
					3.1066332804563217
				]
			]
		},
		{
			"type": "rectangle",
			"version": 308,
			"versionNonce": 1364367829,
			"isDeleted": false,
			"id": "dC6EiShKwIMVA_u9kdfNv",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -512.3062876296802,
			"y": -707.5625257636018,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 594.5455781823032,
			"height": 532.9144870096477,
			"seed": 1868789749,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"id": "UlbTfwHFHuHT6xNfDbzim",
					"type": "arrow"
				}
			],
			"updated": 1698112346377,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 208,
			"versionNonce": 2083260475,
			"isDeleted": false,
			"id": "L6Qlctve",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -505.5004444588675,
			"y": -392.7111285370934,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 66.35992431640625,
			"height": 50,
			"seed": 293801141,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "UlbTfwHFHuHT6xNfDbzim",
					"type": "arrow"
				}
			],
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "DES\nOutlier",
			"rawText": "DES\nOutlier",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "DES\nOutlier",
			"lineHeight": 1.25,
			"baseline": 42
		},
		{
			"type": "arrow",
			"version": 419,
			"versionNonce": 849603829,
			"isDeleted": false,
			"id": "uzCV7THdpWkeF86WzGmyi",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -634.6237708572304,
			"y": -476.3617107090044,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 300.9578813192291,
			"height": 1.1706303331125696,
			"seed": 61487189,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"boundElements": [],
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "zvZanJFU",
				"focus": 0.28138641325224845,
				"gap": 1.6964977526945404
			},
			"endBinding": {
				"elementId": "Mx_GY-o8lQAsjenr7d2mo",
				"gap": 11.276748631034422,
				"focus": -1.4572471771266085
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					300.9578813192291,
					-1.1706303331125696
				]
			]
		},
		{
			"type": "text",
			"version": 238,
			"versionNonce": 1726580981,
			"isDeleted": false,
			"id": "nHeuoLm0",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -305.09556938994604,
			"y": -517.9204643840379,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 46.89772980788664,
			"height": 27.717352398983135,
			"seed": 30802613,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "zpDqcSS44HwJzZ4rdqZW-",
					"type": "arrow"
				}
			],
			"updated": 1698112198830,
			"link": null,
			"locked": false,
			"fontSize": 22.173881919186513,
			"fontFamily": 1,
			"text": "DEP",
			"rawText": "DEP",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "DEP",
			"lineHeight": 1.25,
			"baseline": 18.999999999999993
		},
		{
			"type": "rectangle",
			"version": 99,
			"versionNonce": 1637560565,
			"isDeleted": false,
			"id": "vu6E6l8Pcm7ANlbkns5SG",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -778.6922064059107,
			"y": -403.4587503650322,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 175.35766691127742,
			"height": 60,
			"seed": 625931477,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"boundElements": [
				{
					"type": "text",
					"id": "wlOJI0UY"
				},
				{
					"id": "sA2OHuIwENmN2Luqd_2QJ",
					"type": "arrow"
				}
			],
			"updated": 1698112330695,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 84,
			"versionNonce": 1364413013,
			"isDeleted": false,
			"id": "wlOJI0UY",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -730.9533372447056,
			"y": -398.4587503650322,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 79.87992858886719,
			"height": 50,
			"seed": 1668152597,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [],
			"updated": 1698112330695,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Signed\nRequest",
			"rawText": "Signed\nRequest",
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "vu6E6l8Pcm7ANlbkns5SG",
			"originalText": "Signed\nRequest",
			"lineHeight": 1.25,
			"baseline": 42
		},
		{
			"type": "text",
			"version": 119,
			"versionNonce": 881521589,
			"isDeleted": false,
			"id": "zvZanJFU",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -786.8601703433234,
			"y": -540.2781481296832,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"width": 150.53990173339844,
			"height": 100,
			"seed": 477938165,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"boundElements": [
				{
					"id": "sA2OHuIwENmN2Luqd_2QJ",
					"type": "arrow"
				},
				{
					"id": "uzCV7THdpWkeF86WzGmyi",
					"type": "arrow"
				}
			],
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Contains:\n    1.memId\n    2.DegId\n    3.Signature",
			"rawText": "Contains:\n    1.memId\n    2.DegId\n    3.Signature",
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Contains:\n    1.memId\n    2.DegId\n    3.Signature",
			"lineHeight": 1.25,
			"baseline": 92
		},
		{
			"id": "Mx_GY-o8lQAsjenr7d2mo",
			"type": "rectangle",
			"x": -327.42422841360724,
			"y": -526.0046373907821,
			"width": 90.57465651939663,
			"height": 39.08045275457971,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 1781781109,
			"version": 121,
			"versionNonce": 32900885,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "uzCV7THdpWkeF86WzGmyi",
					"type": "arrow"
				},
				{
					"id": "zpDqcSS44HwJzZ4rdqZW-",
					"type": "arrow"
				}
			],
			"updated": 1698112355245,
			"link": null,
			"locked": false
		},
		{
			"id": "PUv_TLlXznjgKdui1a_X7",
			"type": "diamond",
			"x": -557.3094823198571,
			"y": -546.6943557030128,
			"width": 181.60922346443965,
			"height": 70,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 633815067,
			"version": 154,
			"versionNonce": 1080332987,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "91eqXdmc"
				}
			],
			"updated": 1698112163555,
			"link": null,
			"locked": false
		},
		{
			"id": "91eqXdmc",
			"type": "text",
			"x": -491.7271532603878,
			"y": -524.1943557030128,
			"width": 50.63995361328125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1685055963,
			"version": 11,
			"versionNonce": 1408191093,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"text": "check",
			"rawText": "check",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "PUv_TLlXznjgKdui1a_X7",
			"originalText": "check",
			"lineHeight": 1.25
		},
		{
			"id": "6_Oll3ouDjMzZtj-fqME-",
			"type": "arrow",
			"x": -468.64148855492556,
			"y": -803.2460798409438,
			"width": 3.2860572857148327,
			"height": 258.39081862877174,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1551653211,
			"version": 128,
			"versionNonce": 492340059,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					3.2860572857148327,
					258.39081862877174
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "P8yVIu8A11dLJyx1gLLGX",
				"focus": 0.015752593614342784,
				"gap": 2.7586206896551744
			},
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "P8yVIu8A11dLJyx1gLLGX",
			"type": "rectangle",
			"x": -571.1024173952882,
			"y": -903.0161667213316,
			"width": 206.8965517241379,
			"height": 97.01146619073268,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 639473499,
			"version": 117,
			"versionNonce": 1692939221,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "0W6qy5SV"
				},
				{
					"id": "6_Oll3ouDjMzZtj-fqME-",
					"type": "arrow"
				},
				{
					"id": "sA2OHuIwENmN2Luqd_2QJ",
					"type": "arrow"
				}
			],
			"updated": 1698112163555,
			"link": null,
			"locked": false
		},
		{
			"id": "0W6qy5SV",
			"type": "text",
			"x": -489.00412474855125,
			"y": -867.0104336259652,
			"width": 42.69996643066406,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 322129563,
			"version": 33,
			"versionNonce": 1673879547,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"text": "TDH",
			"rawText": "TDH",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "P8yVIu8A11dLJyx1gLLGX",
			"originalText": "TDH",
			"lineHeight": 1.25
		},
		{
			"id": "e-3GRKMxIi1ZojgYuul8L",
			"type": "rectangle",
			"x": -463.9760367379606,
			"y": -770.142631565082,
			"width": 135.63215584590512,
			"height": 41.37931034482767,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 1040055707,
			"version": 124,
			"versionNonce": 767357237,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "4kRP3qka"
				}
			],
			"updated": 1698112163555,
			"link": null,
			"locked": false
		},
		{
			"id": "4kRP3qka",
			"type": "text",
			"x": -437.9799127334651,
			"y": -761.9529763926681,
			"width": 83.63990783691406,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 960761147,
			"version": 13,
			"versionNonce": 1303226523,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"text": "publicKey",
			"rawText": "publicKey",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "e-3GRKMxIi1ZojgYuul8L",
			"originalText": "publicKey",
			"lineHeight": 1.25
		},
		{
			"id": "sA2OHuIwENmN2Luqd_2QJ",
			"type": "arrow",
			"x": -614.3110673208885,
			"y": -405.18648002125167,
			"width": 70.89252676435683,
			"height": 388.40442740589936,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 947130485,
			"version": 432,
			"versionNonce": 877295893,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112330695,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					70.89252676435683,
					-388.40442740589936
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "vu6E6l8Pcm7ANlbkns5SG",
				"focus": 0.7610856913293507,
				"gap": 1.7277296562194806
			},
			"endBinding": {
				"elementId": "P8yVIu8A11dLJyx1gLLGX",
				"focus": 0.5756390192438237,
				"gap": 12.413793103447972
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "FzVd9JTHUybeNYm5sp0RX",
			"type": "line",
			"x": -348.5737940116675,
			"y": -706.234571557,
			"width": 4.59767308728442,
			"height": 532.4138351966594,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1666453243,
			"version": 101,
			"versionNonce": 818142523,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					4.59767308728442,
					532.4138351966594
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "783j3D7m",
			"type": "text",
			"x": -357.5121061690652,
			"y": -537.1171000695861,
			"width": 24.199981689453125,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1896132661,
			"version": 46,
			"versionNonce": 716963925,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112188786,
			"link": null,
			"locked": false,
			"text": "as",
			"rawText": "as",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 17,
			"containerId": null,
			"originalText": "as",
			"lineHeight": 1.25
		},
		{
			"id": "KVSv2ihn",
			"type": "text",
			"x": -334.09103539097794,
			"y": -391.2920708835087,
			"width": 57.79994201660156,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1196619963,
			"version": 38,
			"versionNonce": 141196763,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"text": "DES\nInside",
			"rawText": "DES\nInside",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 42,
			"containerId": null,
			"originalText": "DES\nInside",
			"lineHeight": 1.25
		},
		{
			"id": "7y6YOPMouY7OXmsRw6Cwg",
			"type": "diamond",
			"x": -197.01061273396067,
			"y": -355.4817589809205,
			"width": 233.1034482758621,
			"height": 148.5057751885775,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 412983739,
			"version": 157,
			"versionNonce": 894894549,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "wbc8S5EW"
				},
				{
					"id": "zpDqcSS44HwJzZ4rdqZW-",
					"type": "arrow"
				},
				{
					"id": "UjkeSu2VCAUxbUcXFLgXo",
					"type": "arrow"
				}
			],
			"updated": 1698112224916,
			"link": null,
			"locked": false
		},
		{
			"id": "wbc8S5EW",
			"type": "text",
			"x": -132.61470214204593,
			"y": -306.35531518377616,
			"width": 104.75990295410156,
			"height": 50,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 162093659,
			"version": 133,
			"versionNonce": 1728228597,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112212809,
			"link": null,
			"locked": false,
			"text": "Check\nAvailability",
			"rawText": "Check\nAvailability",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 42,
			"containerId": "7y6YOPMouY7OXmsRw6Cwg",
			"originalText": "Check\nAvailability",
			"lineHeight": 1.25
		},
		{
			"id": "zpDqcSS44HwJzZ4rdqZW-",
			"type": "arrow",
			"x": -234.58756675896495,
			"y": -504.60339520549564,
			"width": 150.9698168506145,
			"height": 150.4276918304456,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1560951995,
			"version": 349,
			"versionNonce": 1577091707,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112360132,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					150.9698168506145,
					150.4276918304456
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "Mx_GY-o8lQAsjenr7d2mo",
				"focus": -0.7038988558158273,
				"gap": 2.2620051352456585
			},
			"endBinding": {
				"elementId": "7y6YOPMouY7OXmsRw6Cwg",
				"focus": 0.9400215432642953,
				"gap": 1
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "UjkeSu2VCAUxbUcXFLgXo",
			"type": "arrow",
			"x": -179.42524942316732,
			"y": -262.1831434450272,
			"width": 805.0922602668516,
			"height": 4.695094086158633,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 2
			},
			"seed": 1887403195,
			"version": 207,
			"versionNonce": 2006630549,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112365989,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-805.0922602668516,
					-4.695094086158633
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "7y6YOPMouY7OXmsRw6Cwg",
				"focus": -0.2642708629912282,
				"gap": 6.61420395052528
			},
			"endBinding": {
				"elementId": "3edVTvIJqN5ZM8icIVSx7",
				"focus": 0.5055091973405873,
				"gap": 1.5188137230637722
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "rym0WEXQONfqqRQIpLR2c",
			"type": "rectangle",
			"x": -783.1014713851052,
			"y": -302.11429419664853,
			"width": 186.4019301443809,
			"height": 35.119187741592214,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": {
				"type": 3
			},
			"seed": 991481819,
			"version": 121,
			"versionNonce": 387803125,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "TdRKJXF9"
				}
			],
			"updated": 1698112325056,
			"link": null,
			"locked": false
		},
		{
			"id": "TdRKJXF9",
			"type": "text",
			"x": -770.7104504657468,
			"y": -297.0547003258524,
			"width": 161.61988830566406,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 911152955,
			"version": 74,
			"versionNonce": 662085973,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1698112325056,
			"link": null,
			"locked": false,
			"text": "catalog and eds",
			"rawText": "catalog and eds",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "rym0WEXQONfqqRQIpLR2c",
			"originalText": "catalog and eds",
			"lineHeight": 1.25
		},
		{
			"id": "OAKELQpN",
			"type": "text",
			"x": -299.5506932573572,
			"y": -517.585142761876,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 163822843,
			"version": 5,
			"versionNonce": 515683605,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 17,
			"containerId": "Mx_GY-o8lQAsjenr7d2mo",
			"originalText": "",
			"lineHeight": 1.25
		},
		{
			"id": "lg9fASVj",
			"type": "text",
			"x": -938.2289664254606,
			"y": -817.7288384616336,
			"width": 10,
			"height": 25,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 322005627,
			"version": 5,
			"versionNonce": 1296314037,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"text": "",
			"rawText": "",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 17,
			"containerId": null,
			"originalText": "",
			"lineHeight": 1.25
		},
		{
			"id": "uucs_fCbSeQOHCojLc84v",
			"type": "freedraw",
			"x": -233.17146709895212,
			"y": -505.77478741098713,
			"width": 153.1034482758621,
			"height": 19.310344827586164,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 455767509,
			"version": 57,
			"versionNonce": 996344603,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1698112163555,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.45974205280174374,
					-0.919568292025815
				],
				[
					2.7586206896551175,
					-1.3793103448275588
				],
				[
					5.517241379310349,
					-2.2988786368534306
				],
				[
					9.655172413793139,
					-2.7586206896551744
				],
				[
					14.712671740301744,
					-3.218404835668082
				],
				[
					17.47129242995686,
					-3.218404835668082
				],
				[
					20.229913119612092,
					-3.218404835668082
				],
				[
					22.52870757004314,
					-3.218404835668082
				],
				[
					25.287328259698256,
					-3.218404835668082
				],
				[
					28.045948949353487,
					-3.218404835668082
				],
				[
					32.183879983836164,
					-4.137931034482733
				],
				[
					36.321811018318954,
					-4.137931034482733
				],
				[
					39.540257947198256,
					-4.597715180495641
				],
				[
					43.678188981681046,
					-4.597715180495641
				],
				[
					48.73560412176721,
					-5.057499326508605
				],
				[
					53.793103448275815,
					-5.057499326508605
				],
				[
					58.39077653556035,
					-5.057499326508605
				],
				[
					63.448275862068954,
					-5.977025525323256
				],
				[
					70.34482758620686,
					-6.436809671336221
				],
				[
					77.70112136314651,
					-7.356335870150758
				],
				[
					82.75862068965512,
					-8.73564621497843
				],
				[
					88.73560412176721,
					-9.195430360991338
				],
				[
					95.63215584590512,
					-9.195430360991338
				],
				[
					101.14939722521547,
					-9.195430360991338
				],
				[
					107.12646484375,
					-9.195430360991338
				],
				[
					113.10344827586209,
					-9.195430360991338
				],
				[
					118.62068965517244,
					-9.195430360991338
				],
				[
					124.59767308728442,
					-8.275862068965523
				],
				[
					129.1954303609914,
					-7.816120016163779
				],
				[
					132.41379310344826,
					-6.436809671336221
				],
				[
					134.25284550107756,
					-5.977025525323256
				],
				[
					137.47129242995686,
					-5.517241379310349
				],
				[
					139.31034482758616,
					-4.137931034482733
				],
				[
					140.68965517241372,
					-3.6781889816809894
				],
				[
					142.0689655172414,
					-3.218404835668082
				],
				[
					143.44827586206895,
					-2.2988786368534306
				],
				[
					144.36784415409477,
					-1.3793103448275588
				],
				[
					145.74715449892244,
					-0.919568292025815
				],
				[
					146.66663860452582,
					0
				],
				[
					147.58620689655174,
					0.45974205280174374
				],
				[
					148.9655172413793,
					1.8390523976293593
				],
				[
					149.88508553340512,
					2.298836543642267
				],
				[
					150.34482758620686,
					2.7586206896551744
				],
				[
					151.2643958782328,
					4.13793103448279
				],
				[
					151.72413793103442,
					5.057457233297441
				],
				[
					152.18387998383616,
					5.9769834321120925
				],
				[
					152.64370622306035,
					7.356293776939651
				],
				[
					152.64370622306035,
					8.275862068965523
				],
				[
					153.1034482758621,
					8.735604121767267
				],
				[
					153.1034482758621,
					9.195388267780174
				],
				[
					153.1034482758621,
					10.114914466594826
				],
				[
					153.1034482758621,
					10.114914466594826
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				153.1034482758621,
				10.114914466594826
			]
		},
		{
			"id": "fs7nPpv4fxXo_uzP3QPYq",
			"type": "freedraw",
			"x": -118.22896642546073,
			"y": -783.0161930295889,
			"width": 38.1609476023707,
			"height": 2.7586206896551175,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 595802965,
			"version": 111,
			"versionNonce": 1667415125,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1698112166001,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					1.3793103448275588,
					0
				],
				[
					2.7586206896551175,
					0
				],
				[
					7.3563779633620925,
					0
				],
				[
					19.310344827586164,
					0.45976836105865004
				],
				[
					31.264395878232676,
					2.298852328596354
				],
				[
					35.86206896551721,
					2.298852328596354
				],
				[
					37.701205549568954,
					2.7586206896551175
				],
				[
					38.1609476023707,
					2.7586206896551175
				],
				[
					38.1609476023707,
					2.7586206896551175
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				38.1609476023707,
				2.7586206896551175
			]
		},
		{
			"id": "uZMHyThvD_uyoWsFW94W5",
			"type": "freedraw",
			"x": -90.44273785459677,
			"y": -786.2190500993624,
			"width": 0.0001,
			"height": 0.0001,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 481270389,
			"version": 6,
			"versionNonce": 108371835,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1698112166001,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				0.0001,
				0.0001
			]
		},
		{
			"id": "Be6UoJ0f10VTKADMNPir2",
			"type": "freedraw",
			"x": -99.62779347640958,
			"y": -779.7355047030362,
			"width": 0.0001,
			"height": 0.0001,
			"angle": 0,
			"strokeColor": "#1e1e1e",
			"backgroundColor": "transparent",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"frameId": null,
			"roundness": null,
			"seed": 1521832597,
			"version": 6,
			"versionNonce": 2094164405,
			"isDeleted": true,
			"boundElements": null,
			"updated": 1698112166001,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				0.0001,
				0.0001
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#1e1e1e",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"scrollX": 1764.255840923995,
		"scrollY": 1010.9280740623943,
		"zoom": {
			"value": 0.9523503095655059
		},
		"currentItemRoundness": "round",
		"gridSize": null,
		"gridColor": {
			"Bold": "#C9C9C9FF",
			"Regular": "#EDEDEDFF"
		},
		"currentStrokeOptions": null,
		"previousGridSize": null,
		"frameRendering": {
			"enabled": true,
			"clip": true,
			"name": true,
			"outline": true
		}
	},
	"files": {}
}
```
%%