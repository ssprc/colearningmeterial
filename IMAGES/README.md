# colearningmeterial
## issue
23.9.26
新建一个issue：
- 项目构建、版本依赖、ide的问题、项目结构等方面会遇到的bug



00： 语言与数据库：
- Java基础编程语言与语法
- Kotlin
- Go
- JS
- 
- MySQL
- 海量存储
- MongoDB
- FastDFS

10： 中级后端：
- 项目优化
- JVM虚拟机
- 多线程与并发编程
- 网络编程
- Spring- SpringMVC- SpringBoot
- MyBatis
- Tomcat
- Nginx
- Redis
2. ElasticSearch
3. 
20： 高级后端：

3. RocketMQ
4. Kafka
5. Nacos
6. OpenFeign
7. Gateway
8. Sentinel
9. Dubbo
10. Jekins
11. Docker
12. Kubernetes

30： 数据库：MySQL、Mongo、Redis

40： 代码逻辑：业务、算法

90： 其他：与代码无关





