- 什么是协程？它与线程有何不同？
- 为什么需要协程？它解决了什么问题？
- 什么是挂起函数（`suspend`函数）？



1. **什么是协程？它与线程有何不同？**

   - **协程**（Coroutines）是一种轻量级的“线程”（但实际上并不是真正的操作系统线程）。它们是编程语言级别或框架级别的结构，允许多个任务并发执行，但在用户空间中进行调度，而不是在内核空间中。
   
   - **区别**：
     - **资源占用**：协程通常比线程更轻量级，创建和销毁的成本更低。一个进程中可以有数十万的协程，而有这么多的线程可能会导致资源耗尽。
     - **调度**：线程的调度是由操作系统管理的，而协程的调度是由它们所在的程序或框架控制的。
     - **上下文切换**：线程上下文切换涉及到复杂的保存和恢复状态的过程，而协程的切换通常更简单、更快速。
     - **阻塞**：当线程等待某些资源时，它会被阻塞，而协程可以“挂起”，让其他协程在同一线程上继续运行。

2. **为什么需要协程？它解决了什么问题？**

   - **并发处理**：协程提供了一种简单、高效的方式来处理并发任务，特别是I/O密集型任务，如网络请求、数据库操作等。
   
   - **简化异步代码**：传统的异步编程，如回调、Promise、Future等，可能导致“回调地狱”或复杂的错误处理。使用协程，异步代码可以写得像同步代码那样简洁、直观。
   
   - **提高资源效率**：由于协程的轻量级特性，它们能更有效地利用系统资源，处理更多的并发任务。

3. **什么是挂起函数（`suspend`函数）？**

   - 在Kotlin中，挂起函数是一种特殊的函数，它可以在不阻塞当前线程的情况下“暂停”执行，并在稍后的某个时刻“恢复”执行。这使得我们可以在挂起函数中进行长时间运行的操作，如I/O操作，而不会阻塞主线程。
   
   - `suspend`关键字用于声明一个函数为挂起函数。挂起函数只能在另一个挂起函数或协程中调用。
   
   - 挂起函数的一个主要特点是它们有能力挂起协程的执行（不是线程）并允许其他协程在同一线程上运行。



> 我的理解

协程优化在哪里？

当我需要在一个服务中进行多个IO操作的时候（等待）

```kotlin
fun main() {
    fetchData("URL1")
    fetchData("URL2")
    fetchData("URL3")
}

fun fetchData(url: String) {
    // 假设这里是一个长时间的I/O操作，例如网络请求
    // ...
    println("Fetched data from $url")
}
```

这里三个方法阻塞运行，只有一个fetchData完成之后才会执行下一个。

因为当进行IO操作的时候，CPU会自动阻塞当前线程，等待IO操作完成之后才会进行下一步操作，这样没有充分利用cpu的能力，而且太慢了。

因此可以考虑使用多线程进行优化，

```kotlin
public static void main(String[] args) {
    Thread thread1 = new Thread(() -> fetchData("URL1"));
    Thread thread2 = new Thread(() -> fetchData("URL2"));
    Thread thread3 = new Thread(() -> fetchData("URL3"));
    
    thread1.start();
    thread2.start();
    thread3.start();
    
    try {
        thread1.join();
        thread2.join();
        thread3.join();
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
}

public static void fetchData(String url) {
    // 假设这里是一个长时间的I/O操作，例如网络请求
    // ...
    System.out.println("Fetched data from " + url);
}

```

多线程的方式会进行线程的切换，这种线程的创建、切换、销毁比起协程是需要消耗资源的，

而且虽然上边代码 没有写，但是实际上要涉及到线程管理和资源同步的问题，尤其是线程数量增加或者需要共享资源的时候。



下面是使用协程的方式：

```kotlin
fun main() = runBlocking {
    launch {
        fetchData("URL1")
    }
    launch {
        fetchData("URL2")
    }
    launch {
        fetchData("URL3")
    }
}

suspend fun fetchData(url: String) {
    val data = asyncIO {
        // 假设这里是一个长时间的I/O操作，例如网络请求
    }.await()
    println("Fetched data from $url")
}

//在IO调度器上启动一个新的协程来执行I/O密集型任务，并返回一个Deferred来表示异步的结果。
fun <T> asyncIO(block: suspend () -> T): Deferred<T> = 
    GlobalScope.async(Dispatchers.IO) {
        block()
    }

```



**runBlocking**和 **asyncIO{}.await** 

runBlocking 是一个阻塞代码块，确切的说它是一个函数，它保证了再这个函数内执行的协程完成之前，阻塞当前线程。

它在外部表现为一个阻塞函数，需要完成它的功能之后才会执行下一步。

在代码块内的会表现出协程的特性，当`fetchData("URL1")`执行并等待IO的时候，它会切换到另一个协程`fetchData("URL2")` 这样以来，在实现了在等待IO的同时，继续执行其他代码。

看起来很像是多线程，但是它并没有进行线程切换，因为runBlocking内保证了，或者说是保护了协程内的IO操作不被阻塞而产生线程切换，而是让当前线程继续工作。说的正式一点，就是 **协程间的I/O操作是非阻塞的，可以并发进行。**

这里的阻塞代码块概念并不是进程状态里的那个阻塞，而是在获取结果之前不会向下执行代码，当runBlocking内的协程都在等待IO操作的时候，没有可以执行的其他协程代码了。此时该线程虽然逻辑上是空闲的，但对于cpu来说（在操作系统层面上），它仍然是一个正在执行的线程，处于活动状态，不是睡眠或者等待。

如果IO出问题了，或者经常出现长时间等待协程内IO操作的情况出现，这种情况确实是个问题。

>  解决这种高并发，或者是IO密集型的情况，可以学习不少东西
>
> 1. Kotlin协程
> 2. 响应式编程：RxJava，Reactor
> 3. Node.js或者其他事件驱动的框架
> 4. Vert.x:在JVM上运行的事件驱动型框架
> 5. NIO：非阻塞的IO

























