### **Optional**



相当于是Java提供的空安全类型，用于处理函数不一定能返回值的情况

> **为什么要用它**
>
> 因为使用它之后，你就不能直接对返回结果进行操作，需要把他提取出来，这个提取的过程就会涉及到对空值的处理（抛出异常）



#### **如何提取？**

#### get方法

```java
Optional<String> optionalStr = ...;
String str = optionalStr.get();
```

如果`optionalStr`是个空值，那么get方法获取不到，就会抛出`NoSuchElementException`。

> **NPE做错了什么，费这么大劲也要避免抛出它**
>
> **NullPointerException (NPE)**：
>
> - 通常表示试图访问或操作一个为`null`的对象。
> - 在Java中，NPE通常被认为是一个代码质量问题。频繁出现的NPE可能表示开发者未能正确处理`null`值，这可能导致应用程序在运行时出现不稳定或不可预测的行为。
> - 由于NPE在Java开发中非常普遍，它可能代表各种各样的错误原因，因此可能不容易诊断。
>
>  
>
> 它代表的是访问了一个null，是代码的问题
>
> 而**NoSuchElementException** 这个异常，看到就知道，是你自己主动抛出的，代码是没有问题的，一般就是数据库里没查到等其他问题



#### 其他方法

1. **使用`orElse()`或`orElseGet()`** - 提供一个备选值或备选供应源。
   ```java
   String str = optionalStr.orElse("default value");
   ```

2. **使用`ifPresent()`** - 如果值存在，就执行一个操作。

   ```java
   optionalStr.ifPresent(s -> System.out.println(s));
   ```

3. **使用`isPresent()`进行检查** - 显式检查是否存在值。
   ```java
   if (optionalStr.isPresent()) {
       String str = optionalStr.get();
       // ...
   }
   ```

## 关于OrElseThrow

```java
/**
 * If a value is present, returns the value, otherwise throws
 * {@code NoSuchElementException}.
 *
 * @return the non-{@code null} value described by this {@code Optional}
 * @throws NoSuchElementException if no value is present
 * @since 10
 */
public T orElseThrow() {
    if (value == null) {
        throw new NoSuchElementException("No value present");
    }
    return value;
}

/**
 * If a value is present, returns the value, otherwise throws an exception
 * produced by the exception supplying function.
 *
 * @apiNote
 * A method reference to the exception constructor with an empty argument
 * list can be used as the supplier. For example,
 * {@code IllegalStateException::new}
 *
 * @param <X> Type of the exception to be thrown
 * @param exceptionSupplier the supplying function that produces an
 *        exception to be thrown
 * @return the value, if present
 * @throws X if no value is present
 * @throws NullPointerException if no value is present and the exception
 *          supplying function is {@code null}
 */
public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
    if (value != null) {
        return value;
    } else {
        throw exceptionSupplier.get();
    }
}
```



刚刚的例子里的代码

```kotlin
val des = desRepo.findById(desId).orElseThrow { NoSuchElementException("No DES found with ID: $desId") }
```

这里的orElseThrow就是调用了第二个方法。











